Inicio

Definir lado, base, perimetro como reales

// Solicitar al usuario que ingrese la longitud de los lados y la base del triángulo isósceles
Escribir "Ingrese la longitud del lado del triángulo isósceles:"
Leer lado

Escribir "Ingrese la longitud de la base del triángulo isósceles:"
Leer base

// Calcular el perímetro del triángulo isósceles
perimetro = 2 * lado + base

// Mostrar el resultado
Escribir "El perímetro del triángulo isósceles es:", perimetro

Fin